import tornado.web
import jinja2
import os

template_dir = os.path.join(os.path.dirname(__file__),'templates')
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir),
                               autoescape=True)



class Handler(tornado.web.RequestHandler):
    def write(self,*a,**kw):
        tornado.web.RequestHandler.write(self,*a,**kw)

    def render_str(self,template,**params):
        t=jinja_env.get_template(template)
        return t.render(params)

    def render(self,template,**kw):
        self.write(self.render_str(template,**kw))

    def render_main(self):
	self.render('index.html')

    
