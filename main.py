import tornado.ioloop
import tornado.web
from tornado import database
from tornado import websocket
import os
from jinja import Handler
import hashlib
import re
import random
import string
import tornado.escape
import json


USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")

users={}
chatsession={}


def validate_username(username):
    return USER_RE.match(username)

def generate_salt():
    s=''
    for e in random.sample(string.letters,10):
        s+=e
    return s

class MainHandler(Handler):
    def get(self):
        self.render_main()

    def post(self):
	self.render_main()


class CreateUser(tornado.web.RequestHandler):
    def post(self):
        username=self.get_argument('username')
        if validate_username(username):
            if username not in users.values():
                hashing = hashlib.sha256(username+generate_salt()).hexdigest()
                users[hashing]=username
                #self.content_type = 'application/json'
                response={'token':hashing,'username':username}
                #self.write("{'token':'"+hashing+"','username':'"+username+"'}")
                self.set_header("Content-Type", "application/json")
                self.write(tornado.escape.json_encode(response))
                print "created user"
            else:
                response={'error':'username already in use'}
                self.set_header("Content-Type", "application/json")
                self.write(tornado.escape.json_encode(response))
        else:
            response={'error':'invalid username'}
            self.set_header("Content-Type", "application/json")
            self.write(tornado.escape.json_encode(response))
            

class ChatHandler(tornado.web.RequestHandler):
    def post(self):
        token=self.get_argument('token')
        message=self.get_argument('message')

#websocket server listener
class BroadcastHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        token=self.get_argument('token')
        if token in users.keys():
            for e in chatsession.keys():
                reply={'username':'ghost@localhost','message':users[token]+' joined chat'}
                e.write_message(tornado.escape.json_encode(reply))
            chatsession[self]=token
        

    def on_message(self, message):
        #msg=json.loads(broadcast)
       
        if self in chatsession.keys():
            for e in chatsession.keys():
                reply={'username':users[chatsession[self]],'message':message}
                e.write_message(tornado.escape.json_encode(reply))
    def on_close(self):
        username=users[chatsession[self]]
        users.pop(chatsession[self])
        chatsession.pop(self)
        for e in chatsession.keys():
            reply={'username':'ghost@localhost','message':username+' left chat'}
            e.write_message(tornado.escape.json_encode(reply))


        print "WebSocket closed"
    

application = tornado.web.Application([
    (r"/", MainHandler),(r'/websocket',BroadcastHandler),(r'/static/(.*)', tornado.web.StaticFileHandler, {'path': os.path.join(os.getcwd(), 'static')}),(r"/createuser",CreateUser)
])

if __name__ == "__main__":
    application.listen(888)
    tornado.ioloop.IOLoop.instance().start()
